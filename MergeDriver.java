import java.lang.System;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.lang.model.util.ElementScanner14;

class MergeSort{
	
	private static final int MAX_THREADS = 10;
	private static class SortThreads extends Thread{
		SortThreads(Integer[] array, int begin, int end){
			super(()->{
				MergeSort.mergeSort(array, begin, end);
			});
			this.start();
		}
	}
	
	public static void threadedSort(Integer[] array){
		
		long time = System.currentTimeMillis();
		final int length = array.length;
		System.out.println(" The Selected Max_Threads: "+ MAX_THREADS );
		// The threads are created by trying to split the total number of elements in the main list
		// into equal length sequential sub-lists with equal elements in all (or all but one) the threads
		//If input is N and Max_Threads is M, then no. of threads created are to be M or N/M or N/(M-1)
		boolean exact = length%MAX_THREADS == 0;

		int Numberofelementsinthread = exact? length/MAX_THREADS: length/(MAX_THREADS-1);
	
		Numberofelementsinthread = Numberofelementsinthread < MAX_THREADS? MAX_THREADS : Numberofelementsinthread;
		
		final ArrayList<SortThreads> threads = new ArrayList<>();
		int tc=0;
		for(int i=0; i < length; i+=Numberofelementsinthread){
			int beg = i;
			int remain = (length)-i;
			int end = remain < Numberofelementsinthread? i+(remain-1): i+(Numberofelementsinthread-1);
			final SortThreads t = new SortThreads(array, beg, end);
			
			threads.add(t);
		}

		for(Thread t: threads){
			try{

				t.join();
			} catch(InterruptedException ignored){}
		}
		
		for(int i=0; i < length; i+=Numberofelementsinthread){
			int mid = i == 0? 0 : i-1;
			int remain = (length)-i;
			int end = remain < Numberofelementsinthread? i+(remain-1): i+(Numberofelementsinthread-1);
			
			merge(array, 0, mid, end);
		}
		time = System.currentTimeMillis() - (time);
		System.out.println(" The total number of threads created are: "+ tc);
		// for(int i=0; i < length; i+=Numberofelementsinthread){

		// 	if ((length-i)>= Numberofelementsinthread)
		// 		{ System.out.println(" The number of elements in the thread " + (tc+1) + " are: "+ Numberofelementsinthread +" and are from Array Index " + ((tc)*Numberofelementsinthread)+ " to array Index " + (((tc+1)*Numberofelementsinthread)-1));
		// 		  System.out.print( " and the elements are : ");
		// 			for(int p=0;p<Numberofelementsinthread;p++)
		// 				System.out.print(" "+ array[((tc)*Numberofelementsinthread)+p] + " ");
		// 		   System.out.println();		
		// 		}
		// 		else 
		// 		{
		// 		System.out.println(" The number of elements in the thread :" + (tc+1) + " are: "+ (length -i) + " and are from Array Index " + ((tc)*Numberofelementsinthread)+ " to array Index " + (length-1) );
		// 			//int re=length-(tc*Numberofelementsinthread);
		// 			for(int la=0;la<(length-i);la++)
		// 						System.out.print(" "+array[((tc)*Numberofelementsinthread)+la]+ " ");
										
		// 			System.out.println();		
					
		// 		}
		// 		tc++;
		// 	}
		System.out.println("_-----------*********----------_");
		System.out.println("Time spent for custom multi-threaded recursive merge_sort(): "+ time+ "ms");
		System.out.println("_-----------*********----------_");
	}

	public static void mergeSort(Integer[] array, int begin, int end){
		if (begin<end){
			int mid = (begin+end)/2;
			mergeSort(array, begin, mid);
			mergeSort(array, mid+1, end);
			merge(array, begin, mid, end);
		}
	}
	
	public static void merge(Integer[] array, int begin, int mid, int end){
		Integer[] temp = new Integer[(end-begin)+1];
		
		int i = begin, j = mid+1;
		int k = 0;

		while(i<=mid && j<=end){
			if (array[i] <= array[j]){
				temp[k] = array[i];
				i+=1;
			}else{
				temp[k] = array[j];
				j+=1;
			}
			k+=1;
		}

		while(i<=mid){
			temp[k] = array[i];
			i+=1; k+=1;
		}
		
		while(j<=end){
			temp[k] = array[j];
			j+=1; k+=1;
		}

		for(i=begin, k=0; i<=end; i++,k++){
			array[i] = temp[k];
		}
	}
}
public class MergeDriver{
	private static Random random = new Random();
	private static final int size = 10000000;//38;//random.nextInt(100);
	private static final Integer list[] = new Integer[size];
	static {
	for(int i=0; i<size; i++){
		list[i] = i;// random.nextInt(size +(size-1))-(size-1);
	}
	}
   
	public static void main(String[] args){
	// System.out.print("Input = [");
	// for (Integer each: list)
	// 	System.out.print(each+"  ");
    // System.out.print("] \n" +"Input.length = " + list.length + '\n');

	Integer[] arr1 = Arrays.copyOf(list, list.length);
	long t = System.currentTimeMillis();
	Arrays.sort(arr1, (a,b)->a>b? 1: a==b? 0: -1);
	t = System.currentTimeMillis() - t;
	System.out.println("Time spent for system based Arrays.sort(): " + t + "ms");

	Integer[] arr2 = Arrays.copyOf(list, list.length);
	t = System.currentTimeMillis();
	MergeSort.mergeSort(arr2, 0, arr2.length-1);
	t = System.currentTimeMillis() - t;
	System.out.println("_-----------*********----------_");

	System.out.println("Time spent for custom single threaded recursive merge_sort(): " + t + "ms");
	System.out.println("_-----------*********----------_");

	Integer[] arr = Arrays.copyOf(list, list.length);
	MergeSort.threadedSort(arr);
	// System.out.print("Output = [");
	// for (Integer each: arr)
	// 	System.out.print(each+"  ");
	// System.out.print("]\n");
	}
}


   
