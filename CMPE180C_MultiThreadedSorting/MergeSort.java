
import java.lang.System;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

// public class definition using recursive action
public class MergeSort extends RecursiveAction {
	private static int THRESHOLD = 10; // threshold variable
	public final Integer[] array; // input array
	private final Integer[] tmp; // temporary array
	private static int arrayLength; // length of input array
	private int left; // first index in the array
	private int right; // last index in the array
	
	// function initializing the parameters 
	public MergeSort(Integer[] array, Integer[] tmp, int left, int right) {
		this.array = array;
		this.tmp = tmp;
		this.left = left;
		this.right = right;
	}
	
	// execute function, creating the threads and calling compute function
	static void execute(Integer[] array, int left, int right) {
		ForkJoinPool pool = new ForkJoinPool();
		Integer[] tmp = new Integer[array.length];
		MergeSort task = new MergeSort(array, tmp, 0, array.length-1);
		pool.invoke(task);
	}
	
	// logic for the selection sort
	// when the input is less than the threshold then the input array is sorted using selection sort
	public static void selectionSort(Integer[] array)
    {
        int n = array.length;
        for (int i = 0; i < n-1; i++)
        {
            // Find the minimum element in unsorted array
            int min_idx = i;
            for (int j = i+1; j < n; j++)
			{
				if (array[j].compareTo(array[min_idx])<0) // array[j]<(array[min_idx])
				{
					min_idx = j;
				}
			}
            int temp = array[min_idx];
            array[min_idx] = array[i];
            array[i] = temp;
        }
	}

// logic for merging the array at the end in the merge sort algorithm
  public static void merge(Integer[] array, Integer[] tmp, int left, int right) {
		/* copy active part of array to tmp */
		for (int i = left; i <= right; i++)
			tmp[i] = array[i];

		/* merge lists */
		int k = left;
		int mid = left + (right-left) / 2;
		int j = mid+1;
		while (k <= right) {
			if (left > mid)
				array[k++] = tmp[j++];
			else if (j > right)
				array[k++] = tmp[left++];
			else if (tmp[left] < tmp[j])
				array[k++] = tmp[left++];
			else
				array[k++] = tmp[j++];
		}
	}

	// compute function deciding the sorting technique and performing the sorting
	protected void compute() {
		// checking if fist index is greater than last
		if (left >= right) { 
			return; // does nothing and exits directly
		} else if (right - left < THRESHOLD && arrayLength < THRESHOLD ) {
			// System.out.print("This is selection sort\n");
			MergeSort.selectionSort(array);
		} else {
			// System.out.print("This is merge sort\n");
			int middle = left + (right-left) /2;
			MergeSort worker1 = new MergeSort(array, tmp, left, middle);
			MergeSort worker2 = new MergeSort(array, tmp, middle + 1, right);
			invokeAll(worker1, worker2);
			MergeSort.merge(array, tmp, left, right);
		}
	}

	// main function
	public static void main(String[] args){
	  Random random = new Random();
	  int size = random.nextInt(100); // taking the size of an array randomly, less than 100
	  Integer list[] = new Integer[size]; // creating the array with the size selected above
	  for(int i=0; i<size; i++){
		  list[i] =  random.nextInt(size +(size-1))-(size-1);
	  }
	  // printing the input array
	  System.out.print("Input = [");
      for (Integer each: list)
          System.out.print(each+"  ");
      System.out.print("] \n" +"Input.length = " + list.length + '\n');

	  arrayLength = list.length;
	  // time before executing the sorting function
	  long t = System.currentTimeMillis();
	  // function call
	  execute(list, 0, arrayLength-1);
	  // time after done with sorting the input array
	  t = System.currentTimeMillis() - t;
	  System.out.print("Time taken in this approach: " + t + "ms\n");
	  
	  System.out.print("sorted list:");
	  for (int i =0; i <size; i++)
	  {
		System.out.print(list[i] + " ");
	  }
	}
}