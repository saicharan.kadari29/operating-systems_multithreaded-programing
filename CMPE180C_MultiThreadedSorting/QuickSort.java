// importing the necessary packages
import java.lang.System;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

// public class definition using recursive action
public class QuickSort extends RecursiveAction {
	private static int THRESHOLD = 10; // threshold variable
	public int[] array; // input array
	private static int arrayLength; // length of input array
	private int left; // first index in the array
	private int right; // last index in the array
	private int pivotIndex; // index of the pivot element
	
	// function initializing the parameters
	public QuickSort(int[] array, int left, int right) {
		super();
		this.array = array;
		this.left = left;
		this.right = right;
		this.pivotIndex = left + (right-left)/2;
	}
	
	// execute function, creating the threads and calling compute function
	static void execute(int[] array, int left, int right) {
		ForkJoinPool pool = new ForkJoinPool();
		QuickSort task = new QuickSort(array, 0, array.length-1);
		pool.invoke(task);
	}

	// logic for the selection sort
	// when the input is less than the threshold then the input array is sorted using selection sort
	public static void selectionSort(int[] array)
    {
        int n = array.length;
  
        for (int i = 0; i < n-1; i++)
        {
            // Find the minimum element in unsorted array
            int min_idx = i;
            for (int j = i+1; j < n; j++)
			{
				if (array[j] < array[min_idx])
				{
					min_idx = j;
				}
			}
            int temp = array[min_idx];
            array[min_idx] = array[i];
            array[i] = temp;
        }
	}

    // logic for sorting the array at the end in the quick sort algorithm
    public static void sort(int[] array, int left, int right) {
		if (left < right) {
			int pivotIndex = left + (right-left)/2;//pivot generation
			int pivotNewIndex = partition(array, left, right, pivotIndex);//partitioning
			//recursively sorting sublists
			sort(array, left, pivotNewIndex - 1);
			sort(array, pivotNewIndex + 1, right);
		}
	}
	
	// compute function deciding the sorting technique and performing the sorting
	public void compute() {
		if (right - left < THRESHOLD && arrayLength < THRESHOLD ) {
			// System.out.print("This is selction sort\n");
			QuickSort.selectionSort(array);
		} else if (left < right) {
			// System.out.print("This is Quick sort\n");
			int pivotNewIndex = partition(array, left, right, pivotIndex);
			QuickSort worker1 = new QuickSort(array, left, pivotNewIndex - 1);
			QuickSort worker2 = new QuickSort(array, pivotNewIndex + 1, right);
			invokeAll(worker1, worker2);
		}
	}
	
	static int partition(int[] array, int left, int right, int pivotIndex) {
		int pivotValue = array[pivotIndex];
		
		swap(array, pivotIndex, right);
		int storeIndex = left;
		
		for (int i = left; i < right; i += 1) {
			if (array[i] < pivotValue) {
				swap(array, i, storeIndex);
				storeIndex += 1;
			}
		}
		swap(array, storeIndex, right);
		return storeIndex;
	}
	
	static void swap(int[] array, final int a, final int b) {
		final int tmp = array[a];
		array[a] = array[b];
		array[b] = tmp;
	}

    public static void main(String[] args){
		Random random = new Random();
		int size = random.nextInt(100); // taking the size of an array randomly, less than 100
		int list[] = new int[size]; // creating the array with the size selected above
		for(int i=0; i<size; i++){
			list[i] =  random.nextInt(size +(size-1))-(size-1);
		}
		// printing the input array
		System.out.print("Input = [");
		for (Integer each: list)
			System.out.print(each+"  ");
		System.out.print("] \n" +"Input.length = " + list.length + '\n');
  
		arrayLength = list.length;
		// time before executing the sorting function
		long t = System.currentTimeMillis();
		// function call
		execute(list, 0, arrayLength-1);
		// time after done with sorting the input array
		t = System.currentTimeMillis() - t;
		System.out.print("Time taken: " + t + "ms\n");
	  }

}